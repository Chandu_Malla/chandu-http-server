const http = require('http')
const fs = require('fs')
const path = require('path')
const { v4 } = require('uuid')
const statusInfo = http.STATUS_CODES
const port = 3000

const availablePaths = ['/html', '/json', '/uuid', '/status/200', '/delay/3']

const server = http.createServer((req, res) => {

   if (req.url === '/' && req.method === 'GET') {
      res.writeHead(200, 'OK', { 'Content-Type': 'text/html' })
      res.end(`
            <style>
                body {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    flex-direction: column;
                    height: 100vh;
                    margin: 0;
                    background-color: black;
                    color: white;    
                }
                a {
                    color: white;
                }
                li {
                    font-size: 1.3em;
                }
            </style>
            <h1>Welcome to HTTP Drill 1</h1>
            <h2>Available URL Paths:</h2>
            <ul>
                ${availablePaths.map((path) => `<li><a href="${path}">${path}</a></li><br>`).join('')}
            </ul>
        `)

   } else if (req.url === '/html' && req.method === 'GET') {
      res.writeHead(200, 'OK', { 'Content-Type': 'text/html' })
      const filePath = path.join(__dirname, 'public', 'index.html')
      
      fs.readFile(filePath, 'utf8', (err, data) => {
         if (err) {
            console.error('Error reading HTML file:', err)
            res.writeHead(500, 'Internal Server Error', {
               'Content-Type': 'text/plain',
            })
            res.end('Internal Server Error')
            return
         } else {
            res.end(data)
         }
      })

   } else if (req.url === '/json' && req.method === 'GET') {
      res.writeHead(200, 'OK', { 'Content-Type': 'application/json' })
      const filePath = path.join(__dirname, 'public', 'slideshow.json')

      fs.readFile(filePath, 'utf8', (err, data) => {
         if (err) {
            console.error('Error reading JSON file:', err)
            res.writeHead(500, 'Internal Server Error', {
               'Content-Type': 'text/plain',
            })
            res.end('Internal Server Error')
            return
         } else {
            const jsonObj = JSON.parse(data)
            res.end(JSON.stringify(jsonObj, null, 2))
         }
      })

   } else if (req.url === '/uuid' && req.method === 'GET') {
      res.writeHead(200, 'OK', { 'Content-Type': 'application/json' })
      const responseObject = { uuid: v4() }
      res.end(JSON.stringify(responseObject, null, 2))

   } else if (req.url.startsWith('/status/') && req.method === 'GET') {
      const statusCodeParam = req.url.split('/')[2]

      if (!statusInfo.hasOwnProperty(statusCodeParam)) {
         res.writeHead(404, 'Not Found', { 'Content-Type': 'text/plain' })
         res.end(`Status code ${statusCodeParam} does not exist`)
      } else {
         res.writeHead(200, 'OK', { 'Content-Type': 'text/plain' })
         res.end(
            `The Status code is ${statusCodeParam}, and the status code message is "${statusInfo[statusCodeParam]}"`,
         )
      }

   } else if (req.url.startsWith('/delay/') && req.method === 'GET') {
      const delaySeconds = req.url.split('/')[2]

      if (isNaN(+delaySeconds) || !delaySeconds) {
         res.writeHead(400, 'Bad Request', { 'Content-Type': 'text/plain' })
         return res.end(`Please Enter a Number after /delay/ in place of "${delaySeconds}"`)
      }

      res.writeHead(200, 'OK', { 'Content-Type': 'text/plain' })
      setTimeout(() => {
         res.end(`200 OK, Success response after ${delaySeconds} seconds`)
      }, +delaySeconds * 1000)

   } else {
      res.writeHead(404, 'Not Found', { 'Content-Type': 'text/plain' })
      res.end('404 Invalid URL Path address')
   }

})

server.listen(port, () => {
   console.log(`Server is running on http://localhost:${port}`)
})
